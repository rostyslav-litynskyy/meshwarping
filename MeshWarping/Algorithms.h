#pragma once

#include <stdexcept>
#include <algorithm>
#include <chrono>

#include "Renderer.h"
#include "ModelWrapper.h"

float WarpProfileFunc(float dist, float range);

MyMesh::VIter GetClosestPoint(const OpenMesh::Vec3f& from, ModelWrapper& model, float minRequiredDistance);

std::unordered_map<float*, OpenMesh::Vec3f> GetWarpMap(ModelWrapper& secondaryModel, ModelWrapper& mainModel, float warpRange);

void ApplyWarpMap(std::unordered_map<float*, OpenMesh::Vec3f>& warpData, bool revert = false);