#pragma once

#include <vector>

#include <GL\glew.h>
#include <GL\freeglut.h>
#include "ModelWrapper.h"

class Renderer
{

private:

    std::vector<ModelWrapper*> mModels;
    OpenMesh::Vec3f mEye;
    static void FpsTimer(int);

public:

    void Draw();
    void Init(int argc, char* argv[], void drawFunc(void) );
    void AddModelToTheScene(ModelWrapper* model);

};

