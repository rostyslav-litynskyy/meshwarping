#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <chrono>

#include "Renderer.h"
#include "ModelWrapper.h"
#include "Algorithms.h"

float warpRange = 1.5;
Renderer renderer;
std::string mainModelFilePath = "..\\..\\tooth\\2\\Gums_Splited (1).obj";
std::string secondaryModelFilePath = "..\\..\\tooth\\2\\Tooth_11_Temp (1).obj";

ModelWrapper mainModel(
    mainModelFilePath, 
    Matrix::Rotate(M_PI / 2, OpenMesh::Vec3f(1, 0, 0)), 
    0xff, 0xb3, 0xb3);

ModelWrapper secondaryModel(
    secondaryModelFilePath,
    Matrix::Translate(OpenMesh::Vec3f(0, 45, 4))*Matrix::Rotate(-M_PI * .6, OpenMesh::Vec3f(1, 0, 0)),
    0xf8, 0xff, 0xf0
);

Matrix orbit = Matrix::Rotate(0.003, OpenMesh::Vec3f(0, 0, 1));
Matrix upDown = Matrix::Translate(OpenMesh::Vec3f(0, 0, 0.1));

int counter = 0;
void MainLoop()
{
    counter++;
    upDown.At(2, 3) = abs(upDown.At(2, 3)) * (2* ((counter / 60) % 2) - 1);

    auto warpData = GetWarpMap(secondaryModel, mainModel, warpRange);
    ApplyWarpMap(warpData);
    renderer.Draw();
    ApplyWarpMap(warpData, true);

    mainModel.Transform(orbit);
    secondaryModel.Transform(upDown * orbit);
}

int main(int argc, char* argv[])
{
    float effectRadius = 2;
    
    renderer.AddModelToTheScene(&mainModel);
    renderer.AddModelToTheScene(&secondaryModel);
    renderer.Init(argc, argv, MainLoop);

    return 0;
}