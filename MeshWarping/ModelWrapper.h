#pragma once

#include <string>
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <GL\glew.h>

#include "Matrix.h"
#include "AABB.h"

typedef OpenMesh::TriMesh_ArrayKernelT<>  MyMesh;

class ModelWrapper
{
private:

    MyMesh mModel;
    std::unordered_map<const float*, OpenMesh::Vec3f>* mWarpData;
    GLubyte mColor[3];

public:

	ModelWrapper(const std::string& pathStr, const Matrix& initialTransform, GLubyte r, GLubyte g, GLubyte b)
	{
        mColor[0] = r;
        mColor[1] = g;
        mColor[2] = b;

        mWarpData = nullptr;
        mModel.request_vertex_normals();
        OpenMesh::IO::Options opt;
        opt += OpenMesh::IO::Options::VertexNormal;
        if (!OpenMesh::IO::read_mesh(mModel, pathStr, opt))
            throw std::runtime_error("Could not read mesh file: " + std::string(pathStr));
        Transform(initialTransform);
	}

    void SetWarp(std::unordered_map<const float*, OpenMesh::Vec3f>* warp)
    {
        mWarpData = warp;
    }

    void SaveToFile(const std::string& pathStr)
    {
        if (!OpenMesh::IO::write_mesh(mModel, pathStr))
            throw std::runtime_error("Cannot write mesh to file.");
    }

    void Transform(const Matrix& matrix)
    {
        for (auto it = mModel.vertices_begin(); it != mModel.vertices_end(); it++)
        {
            auto& v = mModel.point(*it);
            matrix.Transform(v);
        }
    }

    AABB GetAABB() const
    {
        const auto& firstV = mModel.point(*mModel.vertices_begin());
        AABB res{ firstV, firstV };

        for (auto it = mModel.vertices_begin(); it != mModel.vertices_end(); ++it)
        {
            const auto& v = mModel.point(*it);
            for (int i = 0; i < 3; i++)
            {
                res.From.data()[i] = std::min(res.From.data()[i], v.data()[i]);
                res.To.data()[i] = std::max(res.To.data()[i], v.data()[i]);
            }
        }
        return res;
    }

    MyMesh::FaceIter FacesBegin()
    {
        return mModel.faces_begin();
    }

    MyMesh::FaceIter FacesEnd()
    {
        return mModel.faces_end();
    }

    MyMesh::VIter VerticesBegin()
    {
        return mModel.vertices_begin();
    }

    MyMesh::VIter VerticesEnd()
    {
        return mModel.vertices_end();
    }

    MyMesh::FVIter FaceVerticesBegin(const MyMesh::FaceIter fIt)
    {
        return mModel.fv_begin(*fIt);
    }

    MyMesh::FVIter FaceVerticesEnd(const MyMesh::FaceIter fIt)
    {
        return mModel.fv_end(*fIt);
    }

    const float* GetPointCoords(const MyMesh::FVIter vIt)
    {
        return mModel.point(*vIt).data();
    }

    const float* GetPointNormalCoords(const MyMesh::FVIter vIt)
    {
        return mModel.normal(*vIt).data();
    }

    OpenMesh::Vec3f& GetPoint(const MyMesh::VIter vIt)
    {
        return mModel.point(*vIt);
    }

    const OpenMesh::Vec3f& GetPointNormal(const MyMesh::VIter vIt)
    {
        return mModel.normal(*vIt);
    }

    void ApplyWarp(const float* source, float* dest)
    {
        dest[0] = source[0];
        dest[2] = source[2];
        dest[3] = source[3];

        if (mWarpData == nullptr)
            return;
        
        auto it = mWarpData->find(source);
        if (it != mWarpData->end())
        {
            float* diff = it->second.data();
            dest[0] += diff[0];
            dest[1] += diff[1];
            dest[2] += diff[2];
        }
    }

    GLubyte* GetColor()
    {
        return mColor;
    }
};

