#pragma once

#include <algorithm>
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>

class AABB
{
public:
    OpenMesh::Vec3f From;
    OpenMesh::Vec3f To;

    void Expand(float amount)
    {
        for (int i = 0; i < 3; i++)
        {
            From.data()[i] -= amount;
            To.data()[i] += amount;
        }
    }

    bool PointIsInside(const OpenMesh::Vec3f& v)
    {
        for (int i = 0; i < 3; i++)
            if (v.data()[i] < From.data()[i] || v.data()[i] >= To.data()[i])
                return false;
        return true;
    }
};

