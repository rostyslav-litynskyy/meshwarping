#pragma once

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>

class Matrix
{
private:
    float mData[16] = { 0 };

public:

    Matrix(float diagonal = 1)
    {
        for (int i = 0; i < 4; i++)
            At(i, i) = diagonal;
    }
    float& At(int r, int c)
    {
        return mData[r * 4 + c];
    }

    float At(int r, int c) const
    {
        return mData[r * 4 + c];
    }
    void Transform(OpenMesh::Vec3f& v) const
    {
        auto x = v.data()[0];
        auto y = v.data()[1];
        auto z = v.data()[2];
        v.data()[0] = x * At(0, 0) + y * At(0, 1) + z * At(0, 2) + At(0, 3);
        v.data()[1] = x * At(1, 0) + y * At(1, 1) + z * At(1, 2) + At(1, 3);
        v.data()[2] = x * At(2, 0) + y * At(2, 1) + z * At(2, 2) + At(2, 3);
    }
    //static Matrix Scale(float xScale, float yScale, float zScale);
    static Matrix Rotate(float angleRad, const OpenMesh::Vec3f& axis)
    {
        auto sinV = sin(angleRad);
        auto cosV = cos(angleRad);
        auto l = axis.data()[0];
        auto m = axis.data()[1];
        auto n = axis.data()[2];
        Matrix res;
        res.At(0, 0) = l * l * (1 - cosV) + 1 * cosV;
        res.At(0, 1) = m * l * (1 - cosV) - n * sinV;
        res.At(0, 2) = n * l * (1 - cosV) + m * sinV;

        res.At(1, 0) = l * m * (1 - cosV) + n * sinV;
        res.At(1, 1) = m * m * (1 - cosV) + 1 * cosV;
        res.At(1, 2) = n * m * (1 - cosV) - l * sinV;

        res.At(2, 0) = l * n * (1 - cosV) - m * sinV;
        res.At(2, 1) = m * n * (1 - cosV) + l * sinV;
        res.At(2, 2) = n * n * (1 - cosV) + 1 * cosV;
        return res;
    }

    static Matrix Flip(const OpenMesh::Vec3f& flip)
    {
        Matrix res;
        for (int c = 0; c < 3; c++)
            res.At(c, 3) = flip[c];
        return res;
    }

    static Matrix Translate(const OpenMesh::Vec3f& translation)
    {
        Matrix res;
        for (int c = 0; c < 3; c++)
            res.At(c, 3) = translation[c];
        return res;
    }

    Matrix operator*(const Matrix& other)
    {
        Matrix result(0);
        for (int i = 0; i < 4; ++i)
            for (int j = 0; j < 4; ++j)
                for (int k = 0; k < 4; ++k)
                    result.At(i, j) += At(i, k) * other.At(k, j);
        return result;
    }
};

