#include "Renderer.h"

void Renderer::FpsTimer(int) 
{
    glutPostRedisplay();
    glutTimerFunc(1000 / 60, FpsTimer, 0);
}

void Renderer::Draw()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glPushMatrix();
    glBegin(GL_TRIANGLES);

    for (auto modelPtr : mModels)
    {
        auto& model = *modelPtr;
        for (MyMesh::FaceIter f_it = model.FacesBegin(); f_it != model.FacesEnd(); ++f_it)
        {
            for (MyMesh::FVIter fv_it = model.FaceVerticesBegin(f_it); fv_it != model.FaceVerticesEnd(f_it); fv_it++)
            {
                glColor3ubv(model.GetColor());
                glVertex3fv(model.GetPointCoords(fv_it));
                glNormal3fv(model.GetPointNormalCoords(fv_it));
            }
        }
    }

    glEnd();
    glPopMatrix();
    glFlush();
}

void Renderer::Init(int argc, char* argv[], void drawFunc (void))
{
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
    glutInitWindowPosition(100, 100);
    glutInitWindowSize(700, 700);
    glutCreateWindow("first");

    // Enable Smooth Shading
    glShadeModel(GL_SMOOTH);
    // Black Background
    glClearColor(0.0f, 0.0f, 0.0f, 0.5f);
    // Depth Buffer Setup
    glClearDepth(1.0f);
    // Enables Depth Testing
    glEnable(GL_DEPTH_TEST);
    // The Type Of Depth Testing To Do	
    glDepthFunc(GL_LEQUAL);

    gluPerspective(80, 1, 0.1, 1000);

    mEye = OpenMesh::Vec3f(0, 50, 0);
    gluLookAt(0, 50, 0,	// eye
        0, 0, 0,			// center
        0, 0, 1);			// Top

    glutDisplayFunc(drawFunc);
    Renderer::FpsTimer(0);
    //glutIdleFunc(drawFunc);
    glutMainLoop();
}

void Renderer::AddModelToTheScene(ModelWrapper * model)
{
    mModels.push_back(model);
}
