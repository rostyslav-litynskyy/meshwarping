#include "Algorithms.h"

#include <stdexcept>
#include <algorithm>
#include <chrono>

#include "Renderer.h"
#include "ModelWrapper.h"

float WarpProfileFunc(float dist, float range)
{
    float magnitude = (range - dist) / range;
    magnitude *= magnitude * magnitude;
    return magnitude;
}

MyMesh::VIter GetClosestPoint(const OpenMesh::Vec3f& from, ModelWrapper& model, float minRequiredDistance)
{
    float minDist = std::numeric_limits<float>::max();
    MyMesh::VIter res;
    int skipper = 0;
    for (auto it = model.VerticesBegin(); it != model.VerticesEnd(); ++it)
    {
        if ((skipper++) % 10 != 0)
            continue;

        const auto& p = model.GetPoint(it);
        const auto& n = model.GetPointNormal(it);
        bool earlyBreak = false;
        for (int i = 0; i < 3; i++)
        {
            if (abs(p[i] - from[i]) > minDist)
            {
                //earlyBreak = true;
                break;
            }
        }
        if (earlyBreak)
            continue;

        OpenMesh::Vec3f edge = p - from;
        auto currDist = edge.length();

        if (currDist < minDist)
        {
            minDist = currDist;
            res = it;
        }

        /*edge.normalize();
        if ((edge | n) > 0.8)
            break;*/
    }
    if (minDist > minRequiredDistance)
        return model.VerticesEnd();
    return res;
}

std::unordered_map<float*, OpenMesh::Vec3f> GetWarpMap(ModelWrapper& secondaryModel, ModelWrapper& mainModel, float warpRange)
{
    auto toothBox = secondaryModel.GetAABB();
    toothBox.Expand(warpRange);

    std::unordered_map<float*, OpenMesh::Vec3f> warpData;
    int skipper = 0;
    for (auto vMainIt = mainModel.VerticesBegin(); vMainIt != mainModel.VerticesEnd(); ++vMainIt)
    {
        auto& mainPoint = mainModel.GetPoint(vMainIt);
        if (toothBox.PointIsInside(mainPoint))
        {
            auto closestPointIt = GetClosestPoint(mainPoint, secondaryModel, warpRange);
            if (closestPointIt != secondaryModel.VerticesEnd())
            {
                auto& closestPoint = secondaryModel.GetPoint(closestPointIt);
                auto dist = (mainPoint - closestPoint).length();

                auto& n = mainModel.GetPointNormal(vMainIt);

                auto shift = n * WarpProfileFunc(dist, 3);
                //mainPoint += shift;
                warpData.emplace(mainPoint.data(), shift);
            }
        }
    }
    return warpData;
}

void ApplyWarpMap(std::unordered_map<float*, OpenMesh::Vec3f>& warpData, bool revert)
{
    float multiplier = revert ? -1 : 1;
    for (auto& [coords, dist] : warpData)
    {
        coords[0] += multiplier * dist[0];
        coords[1] += multiplier * dist[1];
        coords[2] += multiplier * dist[2];
    }
}